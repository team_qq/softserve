from django.db import models
from django.contrib.auth.models import User, Group
from django.db.models import Q
# Create your models here.


def is_teacher(self):
    if Group.objects.get(id=1) in self.groups.all():
        return True
    else:
        return False


def is_pupil(self):
    if Group.objects.get(id=2) in self.groups.all():
        return True
    else:
        return False


def __str__(self):
    return self.get_full_name()


User.add_to_class("is_teacher", is_teacher)
User.add_to_class("is_pupil", is_pupil)
User.add_to_class("__str__", __str__)


class Form(models.Model):
    name = models.CharField(max_length=30)
    pupils = models.ManyToManyField(User, related_name='form', limit_choices_to=Q(groups=2))
    form_master = models.ForeignKey(User, related_name='form_master', limit_choices_to=Q(groups=1))

    def __str__(self):
        return self.name


class Auditory(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Lesson(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Schedule(models.Model):
    form = models.ForeignKey(Form)
    teacher = models.ForeignKey(User, limit_choices_to=Q(groups=1))
    auditory = models.ForeignKey(Auditory)
    lesson = models.ForeignKey(Lesson)
    lesson_time = (
        ('8:00', '8:00'),
        ('9:30', '9:30'),
        ('10:10', '10:10'),
        ('11:50', '11:50'),
        ('13:20', '13:20'),
        ('15:00', '15:00'),
        )
    time = models.CharField(max_length=5, choices=lesson_time)
    days = (
        ('1', 'Понедельник'),
        ('2', 'Вторник'),
        ('3', 'Среда'),
        ('4', 'Четверг'),
        ('5', 'Пятница'),
        ('6', 'Суббота'),
        ('7', 'Воскресенье'),
        )
    day = models.CharField(max_length=3, choices=days)

    def __str__(self):
        return self.form.name + ' | ' + self.lesson.name

    class Meta:
        unique_together = (('auditory', 'day', 'time'), ('teacher', 'time', 'day'), ('form', 'time', 'day'))

    @staticmethod
    def get_schedule(user):
        days = [x[0] for x in Schedule.days]
        schedule = []
        for day in days:
            schedule.append(Schedule.objects.filter(Q(teacher=user) | Q(form=user.form.first()),
                                                    day=day).order_by("time").all())
        return schedule


class LessonResult(models.Model):
    date = models.DateTimeField()
    teacher = models.ForeignKey(User, limit_choices_to=Q(groups=1))
    form = models.CharField(max_length=30)

    def __str__(self):
        return self.form + ' ' + self.teacher.get_full_name() + ' ' + self.date.strftime("%d-%m")

    def save(self, *args, **kwargs):
        super(LessonResult, self).save(*args, **kwargs) # Call the "real" save() method.
        form = Form.objects.filter(name=self.form).first()
        for pupil in form.pupils.all():
            result = PupilResult(pupil=pupil, lesson=self)
            result.save()


class PupilResult(models.Model):
    pupil = models.ForeignKey(User)
    absent = models.BooleanField(default=False)
    mark = models.IntegerField(null=True, blank=True)
    lesson = models.ForeignKey(LessonResult, related_name='results')

    def __str__(self):
        return self.pupil.get_full_name() + ' ' + str(self.mark)
