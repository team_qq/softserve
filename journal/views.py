from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from .models import Schedule, LessonResult
from .time import get_weeks
import datetime
# Create your views here.


def user_login(request):
    if request.method == 'POST':
        u = request.POST['username'].lower()
        p = request.POST['password']
        user = authenticate(username=u, password=p)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('/')
            else:
                messages.add_message(request, messages.INFO, 'User is not active')
                return redirect(request.path)
        else:
            messages.add_message(request, messages.INFO, 'Wrong password or email')
            return redirect(request.path)
    else:
        return render(request, 'login.html')


@login_required
def user_logout(request):
    logout(request)
    return redirect('/login/')


@login_required
def index(request):
    schedule = Schedule.get_schedule(request.user)
    days_of_week = {1: 'Понедельник', 2: 'Вторник', 3: 'Среда', 4: 'Четверг', 5: 'Пятница',
                    6: 'Суббота', 7: 'Воскресенье'}
    if request.user.is_teacher():
        return render(request, 'teacher/index.html', {'schedule': schedule, 'days_of_week': days_of_week})
    elif request.user.is_pupil():
        return render(request, 'pupil/index.html', {'schedule': schedule, 'days_of_week': days_of_week})
    else:
        return redirect('/login/')


@login_required()
def lesson(request, lesson_id):
    lesson = Schedule.objects.get(id=lesson_id)
    week = request.GET.get('week', '')
    day = int(lesson.day) - 1
    weeks = get_weeks()
    date = weeks[int(week)][day]
    date += ' ' + lesson.time
    date = datetime.datetime.strptime(date, '%d/%m/%Y %H:%M')
    print(date)
    if not LessonResult.objects.filter(date=date, teacher=request.user, form=lesson.form.name).first():
        lesson_result = LessonResult(date=date, teacher=request.user, form=lesson.form.name)
        lesson_result.save()
    else:
        lesson_result = LessonResult.objects.filter(date=date, teacher=request.user, form=lesson.form.name).first()
    return render(request, 'teacher/lesson.html', {'lesson_result': lesson_result})
