from django.contrib import admin
from .models import *
from easy_select2 import select2_modelform
# Register your models here.

schedule_form = select2_modelform(Schedule, attrs={'width': '250px'})


class ScheduleAdmin(admin.ModelAdmin):
    form = schedule_form

admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(Form)
admin.site.register(Lesson)
admin.site.register(Auditory)
admin.site.register(LessonResult)
admin.site.register(PupilResult)
