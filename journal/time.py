import datetime


def get_weeks():
    last = datetime.datetime.now() - datetime.timedelta(7)

    last_day_1 = last - datetime.timedelta(days=last.weekday())

    dates = {}

    for n_week in range(3):
        dates[n_week] = [(last_day_1 + datetime.timedelta(days=d+n_week*7)).strftime("%d/%m/%Y") for d in range(7)]
    return dates