from django.conf.urls import url
from django.contrib import admin
from journal import views


urlpatterns = [
    url(r'^login/$', views.user_login),
    url(r'^$', views.index),
    url(r'^lesson/(?P<lesson_id>\d+)/$', views.lesson),
    url(r'^logout/$', views.user_logout)
]
